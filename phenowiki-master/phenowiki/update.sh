echo "This script (update.sh) is called when startup.sh is initialized so it has to sleep for a moment before the volumes are ready"
sleep 60

# Setup logo
docker cp 200px-Wikidata-logo.svg.png phenowiki_wikibase_1:/var/www/html/images/200px-Wikidata-logo.svg.png

# Setup local settings
docker cp LocalSettings.php phenowiki_wikibase_1:/var/www/html/LocalSettings.php

# Setup skin https://github.com/lingua-libre/llskin
git clone https://github.com/thingles/foreground.git
docker cp foreground phenowiki_wikibase_1:/var/www/html/skins/foreground

echo "Update finished"
