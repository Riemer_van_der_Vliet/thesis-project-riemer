# Get all strains that are of instance strain and have a genbank entry
curl -G https://query.wikidata.org/sparql --data-urlencode query='
SELECT ?qitem WHERE {
  ?item wdt:P4333 ?acc .
  ?item wdt:P31 wd:Q855769 .
  BIND(REPLACE(STR(?item), ".*/","") AS ?qitem)     
}' | grep -oh -e 'Q[0-9]*' | awk '{print "docker exec phenowiki_wikibase_1 php /var/www/html/extensions/WikibaseImport/maintenance/importEntities.php --entity "$1}' | parallel -j1




# Setup blazegraph sync?
# docker exec phenowiki_wdqs_1 ./runUpdate.sh -h http://wdqs.svc:9999 -- --wikibaseHost nvme1.wurnet.nl --wikibaseScheme http --entityNamespaces 120,122 -s 20180301000000 &

# Get all strains
#docker exec phenowiki_wikibase_1 php /var/www/html/extensions/WikibaseImport/maintenance/importEntities.php --all-properties --query P31:Q855769

