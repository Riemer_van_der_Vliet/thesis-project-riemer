echo "WARNING... THIS WILL RESET THE WIKIBASE DOCKER INSTANCE IN 10 SECONDS"

sleep 10

docker-compose down --volume

docker system prune -f
docker volume prune -f
# And again...
docker system prune -f
docker volume prune -f
