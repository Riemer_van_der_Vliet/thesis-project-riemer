# Get all strains that are of instance strain and have a genbank entry
curl -G https://query.wikidata.org/sparql --data-urlencode query='
SELECT DISTINCT ?pValue WHERE {
  ?item wdt:P31 wd:Q855769 .
  ?item wdt:P4333 ?genbank .
  ?item ?p ?value .
  FILTER(REGEX(STR(?p),"http://www.wikidata.org/prop/"))
  BIND(REPLACE(STR(?p),".*/","") AS ?pValue)
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
}' | grep -oh -e 'P[0-9]*' | awk '{print "docker exec phenowiki_wikibase_1 php /var/www/html/extensions/WikibaseImport/maintenance/importEntities.php --entity "$1}' | parallel -j1




# Setup blazegraph sync?
# docker exec phenowiki_wdqs_1 ./runUpdate.sh -h http://wdqs.svc:9999 -- --wikibaseHost nvme1.wurnet.nl --wikibaseScheme http --entityNamespaces 120,122 -s 20180301000000 &

# Get all strains
#docker exec phenowiki_wikibase_1 php /var/www/html/extensions/WikibaseImport/maintenance/importEntities.php --all-properties --query P31:Q855769

