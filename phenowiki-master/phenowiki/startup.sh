if [ -z "${STY}" ]; then
    echo "This script can only be executed from a screen session"
else
    # Start the update procedure
    ./update.sh &
    # Start the docker
    docker-compose up
fi

